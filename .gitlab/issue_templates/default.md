<!---Use this form to request pre-approval for funding requests for travel and accommodations. Please answer accurately as the information provided will be used in assessing whether the amenity provided complies with applicable laws related to anti-bribery and undue influence. Please expect a two- to four-week processing time.

NOTE: Check that your issue is marked as confidential. This way, only members of the Fedora Council will receive your request. This box should be checked when you submit the issue: "This issue is confidential and should only be visible to team members with at least Reporter access."

If you have questions, send them by email to fca@fedoraproject.org.--->

## Place of work

* **Full name**:
* **Requested travel dates (start & end)**:
* **Name of your employer**:
* **Is your employer a government entity, state-owned entity, or commercial entity (choose one)?**:
* **Country where you work**:
* **Your title at place of work**:


## Amenities

<!---Provide an estimate for daily costs of each expense listed below. That means your daily cost for the amenities, and NOT the total cost. These should be in US dollar (USD) values and include a short description for any relevant fields. Include any related taxes, gratuities, and fees.--->

* **Daily meal budget (only if approved previously by FCA)**:
* **Entertainment (including link(s) to venue)**:
* **Travel**:
    * _Method of travel_: air/rail/bus
    * _Origin city & airport code_:
    * _Destination city & airport code_:
    * _Any connecting flights, and if yes, where?_:
    * _Departure date from origin_:
    * _Return date to origin_:
* **Accommodation (nightly rate)**:
    * _Nightly rate_:
    * _How many nights will you stay, funded by Fedora?_:
* **Conference Fee (including URL to conference agenda)**:


## Additional information

* **Are you a Red Hat associate or contractor?**: yes/no
* **Have you been provided with Fedora/Red Hat funding in the past 12 months?**: yes/no
* **Does your place of work have any rules about the receipt of business amenities? If yes, please describe**:
* **Is your employer a customer of Red Hat, or are there procurement activities, RFPs, or tenders between your employer and Red Hat?**:
* **If yes to the above, do you hold a position of influence over procurement and spending in your organization?**:
* **Is there any additional information that we should be aware of?**:


<!---DO NOT EDIT BELOW THIS LINE!--->

/confidential
