<!--Are you an approved traveler for Flock 2024? This is the final step in your funding journey. Please answer accurately as the information provided will be used for two purposes:

    1. Assessing whether your sponsorship complies with applicable laws related to anti-bribery and undue influence.
    2. Providing relevant information to an external travel agency used by Red Hat to book travel on behalf of non-Red Hat employees.

NOTE: Check that your issue is marked as confidential. This way, only members of the Fedora Council will receive your request. This box should be checked when you submit the issue: "This issue is confidential and should only be visible to team members with at least Reporter access."

If you have questions, send them by email to fca@fedoraproject.org.-->


## About you
<!--These answers help us link this request form to the information you provided previously on Google Forms.-->

* **Full Name**:
* **FAS Username**: ``
* **Preferred email address**:


## About your workplace

* **Name of your employer**:
* **Country where you work**:
* **Your title at place of work**:
* **Is your employer a government entity, state-owned entity, or commercial entity (choose one)?**:


## Additional information

* **Are you a Red Hat associate or contractor?**: yes/no
* **Have you been provided with Fedora/Red Hat funding in the past 12 months?**: yes/no
* **Does your place of work have any rules about the receipt of business amenities? If yes, please describe**:
* **Is your employer a customer of Red Hat, or are there procurement activities, RFPs, or tenders between your employer and Red Hat?**:
* **If yes to the above, do you hold a position of influence over procurement and spending in your organization?**:
* **Is there any additional information that we should be aware of?**:


<!--DO NOT EDIT BELOW THIS LINE!-->

/confidential
